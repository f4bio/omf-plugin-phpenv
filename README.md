<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

# phpenv Plugin for Fish Shell

[![MIT License](https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.2.0-007EC7.svg?style=flat-square)](http://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

almost everything was copied from [scorphus'](https://github.com/scorphus) awesome [pyenv plugin](https://github.com/oh-my-fish/plugin-pyenv) - thanks bro!

<br/>

Use [phpenv](https://github.com/phpenv/phpenv) with [fish shell](https://fishshell.com/) managed by [Oh my fish](https://github.com/oh-my-fish/oh-my-fish) in a very simple way!

## Installation

```fish
omf update  # Just in case omf is outdated. Avoids missing the package.
omf install phpenv
```

> omf (Oh my fish) is a package manager for fish shell. 
> Just like `composer` is for Php and gem is for Ruby

## Usage

Just use phpenv as you see fit:

```fish
phpenv help
```

Don't forget to check the [phpenv docs](https://github.com/phpenv/phpenv)!

# License

[MIT][mit] © [Oh My Fish! Authors][author] et [al][contributors]

[mit]:            http://opensource.org/licenses/MIT
[author]:         https://github.com/oh-my-fish/plugin-phpenv/blob/master/LICENSE
[contributors]:   https://github.com/oh-my-fish/plugin-phpenv/graphs/contributors
[omf-link]:       https://github.com/oh-my-fish/oh-my-fish
[fish-shell]:     https://fishshell.com
[phpenv]:          https://github.com/phpenv/phpenv
[phpenv-docs]:     https://github.com/phpenv/phpenv/blob/master/COMMANDS.md#command-reference

[license-badge]:  https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square
