function phpenv
  set cmd $argv[1]
  set -e argv[1]

  switch "$cmd"
  case activate deactivate rehash shell
    command phpenv "sh-$cmd" $argv | source
  case '*'
    command phpenv "$cmd" $argv
  end
end
