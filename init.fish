set -q PHPENV_ROOT; or set -l PHPENV_ROOT $HOME/.phpenv

set PATH $PHPENV_ROOT/libexec/pyenv $PHPENV_ROOT/libexec $PHPENV_ROOT/shims $PHPENV_ROOT/bin $PATH 2> /dev/null
setenv PYENV_SHELL fish
